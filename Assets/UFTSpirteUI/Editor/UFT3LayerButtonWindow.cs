using UnityEngine;
using System.Collections;
using UnityEditor;

public class UFT3LayerButtonWindow : UFTSpriteWindowBase {
	
	private UFTAtlasEntryMetadata normalStateEntryMetadata;
	private UFTAtlasEntryMetadata onHoverStateEntryMetadata;
	private UFTAtlasEntryMetadata onClickStateEntryMetadata;
			
	private bool isAllTexturesHasSameSize=false;
	
			
	[MenuItem ("Window/UFT 3Layer Button")]
    static void ShowWindow () {    		
		UFT3LayerButtonWindow window= EditorWindow.GetWindow <UFT3LayerButtonWindow>("UFT 3Layer Button");
		window.initializeAtlasMetadataAndMaterial();
    }
	
	
	
	void OnGUI(){		
		renderAtlasMetadata();
		
		EditorGUILayout.BeginHorizontal();
		showMetadataGui(ref normalStateEntryMetadata,"Normal State");
		showMetadataGui(ref onHoverStateEntryMetadata,"On Hover");
		showMetadataGui(ref onClickStateEntryMetadata,"On Click");		
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.Toggle("all textures has the same size",isAllTexturesHasSameSize);
		
		renderSizeMaterialGenerateButton();
		
	}
	
	public override void onGenerateButton(){
		UFT3LayerButton button=createAndSaveObject<UFT3LayerButton>();
		button.normalStateEntryMetadata=normalStateEntryMetadata;
		button.onHoverStateEntryMetadata=onHoverStateEntryMetadata;
		button.onClickStateEntryMetadata=onClickStateEntryMetadata;
		button.applyMetadata(button.normalStateEntryMetadata);
	}	

	
	public override void onAtlasEntryChange(){
		isAllTexturesHasSameSize=isAllStateHasSameSize();
	}
	
	
	private bool isAllStateHasSameSize(){
		return normalStateEntryMetadata !=null &&  
				onHoverStateEntryMetadata !=null &&
				onClickStateEntryMetadata !=null &&
				normalStateEntryMetadata.pixelRect.width == onHoverStateEntryMetadata.pixelRect.width &&
				onHoverStateEntryMetadata.pixelRect.width == onClickStateEntryMetadata.pixelRect.width &&
				normalStateEntryMetadata.pixelRect.height == onHoverStateEntryMetadata.pixelRect.height &&
				onHoverStateEntryMetadata.pixelRect.height == onClickStateEntryMetadata.pixelRect.height; 
	}
	
}
