using UnityEngine;
using System.Collections;
using UnityEditor;

public class UFTSpriteWindow : UFTSpriteWindowBase {
	private UFTAtlasEntryMetadata spriteMetadata;
	private bool addCollider;
	private static string PLAYERPREFS_SPRITE_COLLIDER="uftLayerButton.sprite.addCollider";
	
	[MenuItem ("Window/UFT Sprite or 1LayerButton")]
    static void ShowWindow () {    		
		UFTSpriteWindow window= EditorWindow.GetWindow <UFTSpriteWindow>("UFT Sprite or 1LayerButton");
		window.initializeAtlasMetadataAndMaterial();			
    }
	
	public override void initializeAtlasMetadataAndMaterial(){
		base.initializeAtlasMetadataAndMaterial();
		addCollider=EditorPrefs.GetBool(PLAYERPREFS_SPRITE_COLLIDER,true);
	}
	
	void OnGUI(){		
		
		renderAtlasMetadata();		
		showMetadataGui(ref spriteMetadata,"Normal State");
		bool newValue= EditorGUILayout.Toggle("add collider(make button)",addCollider);		
		if (newValue!=addCollider){
			addCollider=newValue;
			EditorPrefs.SetBool(PLAYERPREFS_SPRITE_COLLIDER,addCollider);
		}
		
		renderSizeMaterialGenerateButton();
		
	}
	
	

	public override void onGenerateButton(){	
	 	UFTSprite sprite=createAndSaveObject<UFTSprite>( addCollider);		
		sprite.spriteMetadata=spriteMetadata;
		sprite.applyMetadata(sprite.spriteMetadata);		
	}
	
}
