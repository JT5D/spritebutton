using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(UFT2LayerSprite))]
public class UFT2LayerSpriteEditor : Editor {
	private bool changeSize=false;
	private Vector2 newSize;
 	private static string[] layerOptions= new string[] {"main", "spare"};	
	
	public override void OnInspectorGUI(){
		UFT2LayerSprite targetObject=(UFT2LayerSprite)target;
		GUI.enabled=(targetObject.atlasMetadata!=null);		
		EditorGUILayout.ObjectField("atlasMetadata", targetObject.atlasMetadata,typeof(UFTAtlasEntryMetadata));
		
		EditorGUILayout.BeginHorizontal();
		UFTAtlasEntryMetadata newEntryMeta = UFTEditorGUILayout.showAtlasEntryMetadataWithCaption("main"  , targetObject.atlasMetadata,targetObject.mainStateEntryMetadata);
		if (newEntryMeta != targetObject.mainStateEntryMetadata){
			targetObject.mainStateEntryMetadata =newEntryMeta;
			targetObject.onAtlasMigrate();
		}
		
		targetObject.spareStateEntryMetadata = UFTEditorGUILayout.showAtlasEntryMetadataWithCaption("spare", targetObject.atlasMetadata,targetObject.spareStateEntryMetadata);
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.Vector2Field("current size",UFTMeshUtil.getPlaneObjectSize(targetObject.gameObject));
		
		
		bool newChangeSize = EditorGUILayout.Foldout(changeSize,"change mesh size");
		if (newChangeSize == true && changeSize==false){
			newSize=getSizeFromState(targetObject.mainStateEntryMetadata);
		}		
		changeSize=newChangeSize;			
		if (changeSize)
			showChangeSizeGUIBlock(targetObject);
	}
	
	
	private Vector2 getSizeFromState(UFTAtlasEntryMetadata entryMetadata){
		return new Vector2( entryMetadata.pixelRect.width, entryMetadata.pixelRect.height);	
	}
	
	
	
	public void showChangeSizeGUIBlock (UFT2LayerSprite targetObject)
	{		
		
		int choosenOne = EditorGUILayout.Popup("readObject from button", -1, layerOptions);
		
		switch (choosenOne) {
		case 0:
			newSize=getSizeFromState(targetObject.mainStateEntryMetadata);
			break;
		case 1:
			newSize=getSizeFromState(targetObject.spareStateEntryMetadata);
			break;					
		}
		
		newSize = EditorGUILayout.Vector2Field ("new size",newSize);
		
		Rect buttonRect=EditorGUILayout.BeginHorizontal();
		if ( GUI.Button(buttonRect, GUIContent.none) ){
			updateMeshSize(targetObject);			
		}		
		GUILayout.Label ("Update Mesh Size");
		EditorGUILayout.EndHorizontal();
	}

	public void updateMeshSize (UFT2LayerSprite targetObject)
	{
		UFTMeshUtil.updateMesh(targetObject.gameObject, newSize.x, newSize.y);
		targetObject.onAtlasMigrate();
	}
}
