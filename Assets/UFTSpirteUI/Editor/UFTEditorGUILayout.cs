using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class UFTEditorGUILayout {
	

	

	
	static Dictionary<string,UFTAtlasEntryMetadata> resultDict=new Dictionary<string,UFTAtlasEntryMetadata>();
	
	
	public delegate void OnUFTAtlasEntryClick(UFTAtlasMetadata atlasMetadata, UFTAtlasEntryMetadata entryMetadata, Vector2 parentWindowPosition, string parentWindowKey);
	
	
	
	public static UFTAtlasEntryMetadata showAtlasEntryMetadataWithCaption( string                caption,
														    UFTAtlasMetadata      atlasMetadata, 
														    UFTAtlasEntryMetadata atlasEntryMetadataobj, 
														    int?                  parentWindowInstanceId = null){
		EditorGUILayout.BeginVertical(UFTSpriteUtil.verticalScrollViewFixedWidthStyle);
		EditorGUILayout.LabelField(caption, UFTSpriteUtil.middleCenterLabelStyle);
		UFTAtlasEntryMetadata newAtlasEntryMeta = showAtlasEntryMetadata (atlasMetadata,atlasEntryMetadataobj,parentWindowInstanceId);				
		EditorGUILayout.EndVertical();
		return newAtlasEntryMeta;
	}
	
	
	public static UFTAtlasEntryMetadata showAtlasEntryMetadata(UFTAtlasMetadata      atlasMetadata, 
														  UFTAtlasEntryMetadata atlasEntryMetadataobj, 
														  int?                  parentWindowInstanceId = null){
		
		return renderAtlasEntryGUI(atlasMetadata,atlasEntryMetadataobj,showAtlasEntryChooseWindow, parentWindowInstanceId);
	}		
		
	/*
	 * Directly render showAtlasEntryMetadata, with custom delegate OnUFTAtlasEntryClick
	 */	
	public static UFTAtlasEntryMetadata renderAtlasEntryGUI(UFTAtlasMetadata    atlasMetadata, 
													      UFTAtlasEntryMetadata atlasEntryMetadata, 
														  OnUFTAtlasEntryClick  onUFTAtlasEntryClick, 
														  int?                  parentWindowInstanceId = null){
		
		Rect rt = GUILayoutUtility.GetRect(UFTSpriteUtil.SPRITE_RECT_WIDTH,UFTSpriteUtil.SPRITE_RECT_HEIGH,UFTSpriteUtil.rectStyle);
		rt.width = UFTSpriteUtil.SPRITE_RECT_WIDTH;	
		
		string controlKey=parentWindowInstanceId+rt.ToString();
		
		if (resultDict.ContainsKey(controlKey)){		
			atlasEntryMetadata=resultDict[controlKey];			
			resultDict.Remove(controlKey);			
		}
		
		
		Rect textureRect = new Rect(rt.x+UFTSpriteUtil.SPRITE_MARGIN,
								rt.y + UFTSpriteUtil.SPRITE_MARGIN,
								rt.width - (UFTSpriteUtil.SPRITE_MARGIN * 2),
								rt.height - (UFTSpriteUtil.SPRITE_MARGIN * 3) - UFTSpriteUtil.SPRITE_TEXT_HEIGHT);
		
			
			
		Rect labelRect = new Rect( rt.x + UFTSpriteUtil.SPRITE_MARGIN,
								rt.y + rt.height - UFTSpriteUtil.SPRITE_TEXT_HEIGHT - UFTSpriteUtil.SPRITE_MARGIN,
								rt.width - (UFTSpriteUtil.SPRITE_MARGIN * 2),
								UFTSpriteUtil.SPRITE_TEXT_HEIGHT);
		
			
		string tooltipText="";//"controlKey="+controlKey+"\n";
		
		if (atlasEntryMetadata !=null){
			tooltipText=tooltipText + atlasEntryMetadata.name      + "\n" +
							 atlasEntryMetadata.assetPath 		   + "\n" +
							 atlasEntryMetadata.pixelRect 		   + "\n" +
							 atlasEntryMetadata.uvRect    		   + "\n" +
							 "isTrimmed = "+atlasEntryMetadata.isTrimmed;				
		}
					
		
		if (GUI.Button(rt,new GUIContent("", tooltipText)) && onUFTAtlasEntryClick != null){
			Vector2 parentWindowPosition=Vector2.zero;
			if (parentWindowInstanceId!=null){
				EditorWindow window=(EditorWindow) EditorUtility.InstanceIDToObject((int)parentWindowInstanceId);
				parentWindowPosition=new Vector2(window.position.x,window.position.y);
			}
			 onUFTAtlasEntryClick(atlasMetadata,atlasEntryMetadata,parentWindowPosition, controlKey);				
		}
		
		if (atlasEntryMetadata != null){		
			
			float factor= atlasEntryMetadata.pixelRect.width / atlasEntryMetadata.pixelRect.height;			
			if (factor > 1){
				
				float newHeight=textureRect.height / factor;
				textureRect.y=textureRect.y + ((textureRect.height - newHeight) / 2);
				textureRect.height=newHeight;
			} else  {
				float newWidth= textureRect.width * factor;
				textureRect.x = textureRect.x + ((textureRect.width  - newWidth) / 2);
				textureRect.width = newWidth;		
				
			}		
			GUI.DrawTextureWithTexCoords(textureRect,atlasMetadata.texture,atlasEntryMetadata.uvRect);		
			GUI.Box(textureRect,GUIContent.none,UFTTextureUtil.borderStyle);
			
			GUI.Label(labelRect,atlasEntryMetadata.name,UFTSpriteUtil.middleLeftLabelStyle);
		} else {
			
			GUI.Box(textureRect,new GUIContent("x","select atlasEntryMetadata from atlas"),UFTSpriteUtil.boxStubStyle);			
			GUI.Label(labelRect, "(None)",UFTSpriteUtil.middleCenterLabelStyle);
		}
		
		return atlasEntryMetadata;
	}
	
	
	
	
	
	private static void showAtlasEntryChooseWindow(UFTAtlasMetadata      atlasMetadata, 
												   UFTAtlasEntryMetadata entryMetadata, 
												   Vector2  			 parentWindowPosition,
												   string                controlKey){
		
		
		UFTSpriteChooseWindow window=UFTSpriteChooseWindow.Initialize(atlasMetadata, entryMetadata, parentWindowPosition, controlKey);
		window.onSpriteChooserClick+=onSpriteChooserClick;
	}

	private static void onSpriteChooserClick (UFTAtlasEntryMetadata atlasEntryMetadata, string parentControlKey)
	{		
		resultDict.Add(parentControlKey,atlasEntryMetadata);
	}
			
	
	

	
}
