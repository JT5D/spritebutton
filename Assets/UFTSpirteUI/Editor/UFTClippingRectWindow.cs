using UnityEngine;
using System.Collections;
using UnityEditor;

public class UFTClippingRectWindow : ScriptableWizard {
	public int width;
	public int height;
	
	
 	[MenuItem ("Window/UFT Clipping Rect")]
    static void CreateWizard () {
        UFTClippingRectWindow wizard= ScriptableWizard.DisplayWizard<UFTClippingRectWindow>("Clipping Rect", "Create");        
		wizard.width=800;
		wizard.height=600;
    }
	
	 void OnWizardCreate () {
        GameObject go = new GameObject ("ClippingRect");
        UFTClippingRect cr= go.AddComponent<UFTClippingRect>();
        cr.width=width;
		cr.height=height;
    }  

}
