using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class UFTGrafitiDataCollection : UFTGridDataCollectionInt {
	
	[SerializeField]
	public List<UFTGraffitiMetadata> collection;
	
	
	
	public UFTGrafitiDataCollection (List<UFTGraffitiMetadata> collection)
	{
		this.collection = collection;
	}


	

	
	#region implemented abstract members of UFTGridDataCollectionInt
	public override IEnumerator getEnumerator ()
	{
		return collection.GetEnumerator();
	}
	#endregion
}