
using UnityEngine;

public abstract class UFTDataGridSelectorInt :ScriptableObject  {

	public abstract void initializeItem(Object prefab, System.Object metaObject, Vector3 position, UFTClippingRect clippingRect, int index);
}
