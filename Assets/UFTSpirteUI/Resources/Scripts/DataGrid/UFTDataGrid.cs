using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum UFTGridType{
	HORIZONTAL,
	VERTICAL		
}

public class UFTDataGrid : MonoBehaviour {
	
	
	
	public GameObject viewPrefab;
	
	public UFTGridDataCollectionInt gridCollection;	
	public UFTDataGridSelectorInt gridSelectorScript;	
	public UFTClippingRect clippingRect;
	
	public Vector3 pivotPoint;
	public float columnWidth;
	public float rowHeight;
	public int columnCount;
	public int rowCount;
	
	public UFTGridType gridType;
	
	
	public void regenerateGrid(){
		Vector3 objPosition=pivotPoint;
		int rowNumber=0;
		int columnNumber=0;
		IEnumerator enumerator = gridCollection.getEnumerator();
		int i=0;
		while (enumerator.MoveNext()) {
			GameObject obj = (GameObject) Instantiate(viewPrefab);			
			obj.transform.parent=(this.transform);
			
			objPosition=new Vector3(columnNumber * columnWidth, rowNumber * rowHeight, 0);
			obj.transform.position=objPosition;
			
			if (gridSelectorScript!=null)
				gridSelectorScript.initializeItem(obj, enumerator.Current, objPosition, clippingRect, i);			
			
			switch(gridType){
			case UFTGridType.HORIZONTAL:
				rowNumber++;
				if ((rowNumber % rowCount)==0){
					columnNumber++;
					rowNumber=0;
				}										
				break;
			case UFTGridType.VERTICAL:
				columnNumber++;
				if ((columnNumber % columnCount)==0){
					rowNumber++;
					columnNumber=0;
				}
				break;
			default:
				throw new System.Exception("unsuported grid type");
			}
			i++;
			
		}
		
	}
	
}
