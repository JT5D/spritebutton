using UnityEngine;
using System.Collections;



public class UFTSprite : UFTSpriteBase, UFTOnAtlasMigrateInt {	
	public UFTAtlasEntryMetadata spriteMetadata;
	
	#region UFTOnAtlasMigrateInt implementation
	public void onAtlasMigrate ()
	{
		applyMetadata(spriteMetadata);
	}
	#endregion
}
