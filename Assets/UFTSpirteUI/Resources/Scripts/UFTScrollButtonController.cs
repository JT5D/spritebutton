using UnityEngine;
using System.Collections;

public enum ScrollType{
	BY_INDEX,
	BY_OFFSET
}


public delegate void OnScrollBarPageClick(int pageNumber);
public delegate void OnScrollBarOffsetClick(int offset);

public class UFTScrollButtonController : MonoBehaviour {
	public ScrollType scrollType;
	public int pageNumber;
	public int offset;
		
	public OnScrollBarPageClick   onScrollBarPageClick   ;
	public OnScrollBarOffsetClick onScrollBarOffsetClick ;
	
	public UFT2LayerSprite spriteController;
	
 	void OnMouseDown() {
		switch (scrollType){
		case ScrollType.BY_INDEX:
			if (onScrollBarPageClick!=null)
				onScrollBarPageClick(pageNumber);
			break;
		case ScrollType.BY_OFFSET:
			if (onScrollBarOffsetClick!=null)
				onScrollBarOffsetClick(offset);
			break;
		default:
			throw new System.Exception("unsupported scroll type");			
		}
	}
	
	public void subscribeToPageChange(UFTScrollBarController scrollBarController){
		scrollBarController.onPageChange+=onPageChange;	
	}
	
	
	void onPageChange (int pageNumber, UFTScrollBarController scrollBarController)
	{
		switch (scrollType){
		case ScrollType.BY_INDEX:	
			if (pageNumber == this.pageNumber){
				transform.collider.enabled = false;	
				spriteController.applyMetadata(spriteController.mainStateEntryMetadata);
			} else {
				transform.collider.enabled = true;
				spriteController.applyMetadata(spriteController.spareStateEntryMetadata);
			}	
			break;
		case ScrollType.BY_OFFSET:
			if (offset <0){
				if (pageNumber >0){
					transform.collider.enabled = true;					
					spriteController.applyMetadata(spriteController.mainStateEntryMetadata);
				} else {
					transform.collider.enabled = false;					
					spriteController.applyMetadata(spriteController.spareStateEntryMetadata);
				}			
			} else {
				if (pageNumber < (scrollBarController.numberOfPage-1)){
					transform.collider.enabled = true;					
					spriteController.applyMetadata(spriteController.mainStateEntryMetadata);
				} else {
					transform.collider.enabled = false;					
					spriteController.applyMetadata(spriteController.spareStateEntryMetadata);
				}
			}
			break;
		default:
			throw new System.Exception("unsupported type");				
		}
		
	}
}
