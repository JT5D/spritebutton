using UnityEngine;
using System.Collections;


public class UFT3LayerButton : UFTSpriteBase, UFTOnAtlasMigrateInt {
	
	public UFTAtlasEntryMetadata normalStateEntryMetadata;
	public UFTAtlasEntryMetadata onHoverStateEntryMetadata;
	public UFTAtlasEntryMetadata onClickStateEntryMetadata;	
	
	void OnMouseEnter(){
		base.OnMouseEnter();
		applyMetadata(onHoverStateEntryMetadata);	
	}
	
	void OnMouseExit(){
		applyMetadata(normalStateEntryMetadata);	
	}
	
	void OnMouseDown(){
		base.OnMouseDown();
		applyMetadata(onClickStateEntryMetadata);	
	}
	
	void OnMouseUp(){
		applyMetadata(normalStateEntryMetadata);	
	}
		
	
	#region UFTOnAtlasMigrateInt implementation
	public void onAtlasMigrate ()
	{
		applyMetadata(normalStateEntryMetadata);
	}
	#endregion
}
