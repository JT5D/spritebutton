using UnityEngine;
using System.Collections;

public class UFT2LayerSprite : UFTSpriteBase, UFTOnAtlasMigrateInt {

	public UFTAtlasEntryMetadata mainStateEntryMetadata;
	public UFTAtlasEntryMetadata spareStateEntryMetadata;
	
	
	
	
	#region UFTOnAtlasMigrateInt implementation
	public void onAtlasMigrate ()
	{
		applyMetadata(mainStateEntryMetadata);
	}
	#endregion
}
