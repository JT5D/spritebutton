using System;

[Serializable]
public class UFTBorderWidth{
	public float top;
	public float down;
	public float left;
	public float right;
	
	public UFTBorderWidth (){}
	
	public UFTBorderWidth (float top, float down, float left, float right)
	{
		this.top   = top   ;
		this.down  = down  ;
		this.left  = left  ;
		this.right = right ;
	}
	
	public UFTBorderWidth (UFTBorderWidth borderWidth){
		getValuesFrom(borderWidth);
	}
	
	public void getValuesFrom(UFTBorderWidth borderWidth){
		this.top   = borderWidth.top   ;
		this.left  = borderWidth.left  ;
		this.right = borderWidth.right ;
		this.down  = borderWidth.down  ;
	}

}