using UnityEngine;
using System.Collections;


/*
 * vertex order 
 * http://storage7.static.itmages.com/i/13/0410/h_1365577853_7353617_d41d8cd98f.png
 */

public class UFTFlexibleBorderMeshUtil {
	private static int[] leftLinePoints  = new int[] {1 , 2 , 14, 15 };
	private static int[] rightLinePoints = new int[] {7 , 6 , 10, 9  };
	private static int[] topLinePoints   = new int[] {3 , 2 , 6 , 5  };
	private static int[] downLinePoints  = new int[] {13, 14, 10, 11 };
	
	
	private static int[] leftEdgePoints  = new int[] {0 , 3 , 13, 12 };
	private static int[] rightEdgePoints = new int[] {4 , 5 , 11, 8  };
	private static int[] topEdgePoints   = new int[] {0 , 1 , 7 , 4  };
	private static int[] downEdgePoints  = new int[] {12, 15, 9 , 8  };
	
	
	public static Mesh createBorderMesh(float                 width                        , 
										float                 height                       , 
										UFTBorderWidth        vertexBorderWidth            , 
										UFTBorderWidth        uvBorderWidthInPixels        , 
										UFTAtlasEntryMetadata spriteMetadata        = null ,
										float                 textureWidth          = 1024 ,
										float                 textureHeight         = 1024 ){
		Mesh mesh = new Mesh();
		float tempBorderWidthForAll=5;
		Vector3[] vertices= createVertexes (width, height,vertexBorderWidth);
		int[] triangles = createTriangles();
				
		Vector2[] uvs = createUV(width,height,uvBorderWidthInPixels,spriteMetadata,textureWidth,textureHeight);
		
		
		mesh.vertices=vertices;
		mesh.triangles=triangles;
		mesh.uv = uvs;
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
		return mesh;
	}

	public static Vector2[] createUV (  float                 width                 , 
										float                 height                , 
										UFTBorderWidth        uvBorderWidthInPixels , 
										UFTAtlasEntryMetadata spriteMetadata        ,
										float                 textureWidth          ,
										float                 textureHeight         ){
		
		Vector2[] uvs= new Vector2[16];
		for (int i = 0; i < 16; i++) {
			uvs[i]=new  Vector2(0,0);
		}
		
		float perPixelWidth;
		float perPixelHeigh;
		
		float leftEdgeX    ;
		float rightEdgeX   ;
		float topEdgeY     ;
		float downEdgeY    ;
		
		if (spriteMetadata==null || spriteMetadata.name.Equals("")){
			perPixelWidth = 1 / textureWidth  ;
			perPixelHeigh = 1 / textureHeight ;
			
			leftEdgeX  = 0 ;
			rightEdgeX = 1 ;
			topEdgeY   = 1 ;
			downEdgeY  = 0 ;
			
		} else {
			perPixelWidth = spriteMetadata.uvRect.width  / spriteMetadata.pixelRect.width  ;
			perPixelHeigh = spriteMetadata.uvRect.height / spriteMetadata.pixelRect.height ;
			
			leftEdgeX  = spriteMetadata.uvRect.x                                ;
			rightEdgeX = spriteMetadata.uvRect.x + spriteMetadata.uvRect.width  ;
			topEdgeY   = spriteMetadata.uvRect.y + spriteMetadata.uvRect.height ;
			downEdgeY  = spriteMetadata.uvRect.y                                ;
		}	
		float leftLineX  = leftEdgeX  + uvBorderWidthInPixels.left  * perPixelWidth ;
		float rightLineX = rightEdgeX - uvBorderWidthInPixels.right * perPixelWidth ;
		float topLineY   = topEdgeY   - uvBorderWidthInPixels.top   * perPixelHeigh ;
		float downLineY  = downEdgeY  + uvBorderWidthInPixels.down  * perPixelHeigh ;
		
		for (int i = 0; i < 4; i++) {
			uvs[leftLinePoints [i]].x = leftLineX  ;
			uvs[rightLinePoints[i]].x = rightLineX ;
			uvs[topLinePoints  [i]].y = topLineY   ;
			uvs[downLinePoints [i]].y = downLineY  ;
			
			uvs[topEdgePoints  [i]].y = topEdgeY   ;
			uvs[downEdgePoints [i]].y = downEdgeY  ;
			uvs[leftEdgePoints [i]].x = leftEdgeX  ;
			uvs[rightEdgePoints[i]].x = rightEdgeX ;
		}
		return uvs;
	
		
	}

	/*
	public static Vector2[] createUV (float width, float height, Vector3[] vertices)
	{
		float halfWidth  = width/2;
		float halfHeight = height/2;
		Vector2[] result = new Vector2[vertices.Length];
		for (int i = 0; i < vertices.Length; i++) {
			Vector3 vertex=vertices[i];
			float u=(vertex.x+halfWidth)/width;
			float v=(vertex.y+halfHeight)/height;				
			result[i]=new Vector2(u,v);
		}
		return result;
		
	}
	*/
	
	
	
	public static Vector3[] createVertexes (float width, float height, UFTBorderWidth vertexBorderWidth)
	{
		float halfWidth  = width  / 2;
		float halfHeight = height / 2;
		float tempBorderWidth = 10f;
		Rect topLeft=new Rect(-halfWidth,halfHeight,tempBorderWidth,tempBorderWidth);
		Vector3[] topLeftVectors     = rectToVectorArray        ( topLeft            );
		Vector3[] topRigthVectors    = reflectPointsHorizontaly ( topLeftVectors     );
		Vector3[] bottomRightVectors = reflectPointsVerticaly   ( topRigthVectors    );
		Vector3[] bottomLeftVectors  = reflectPointsHorizontaly ( bottomRightVectors );
		
		Vector3[] result=new Vector3[16];
		topLeftVectors     .CopyTo(result, 0  );
		topRigthVectors    .CopyTo(result, 4  );
		bottomRightVectors .CopyTo(result, 8  );
		bottomLeftVectors  .CopyTo(result, 12 );
		
		result = adjustVertexPosition(width, height, result, vertexBorderWidth);
		return result;		
	}
	
	private static Vector3[] adjustVertexPosition (float width, float height, Vector3[] vertices, UFTBorderWidth vertexBorderWidth){
		
		float leftLineX  = -(width  /2 - vertexBorderWidth.left  ) ;
		float rightLineX =  (width  /2 - vertexBorderWidth.right ) ;
		float topLineY   =  (height /2 - vertexBorderWidth.top    ) ;
		float downLineY  = -(height /2 - vertexBorderWidth.down   ) ;
		
		for (int i = 0; i < 4; i++) {
			vertices[leftLinePoints [i]].x = leftLineX  ;
			vertices[rightLinePoints[i]].x = rightLineX ;
			vertices[topLinePoints  [i]].y = topLineY   ;
			vertices[downLinePoints [i]].y = downLineY  ;
		}
		
		return vertices;
	}

	
	public static int[] createTriangles(){
		int[] result=new int[54];
		int[] cornerTriangles= createCornerTriangles();
		int[] connectionTriangles=createConnectionTriangles();
		int[] centerTriangles=createCenterTriangles();
		cornerTriangles.CopyTo(result,0);
		connectionTriangles.CopyTo(result,24);
		centerTriangles.CopyTo(result,48);
		return result;
	}
	
	private static int[] createConnectionTriangles(){
		int[] result=new int[24];
		int iterator=0;
		for (int i = 0; i < 16; i+=4) {
			int p1=i+1;
			int p2=i+7;
			int p3=i+6;
			int p4=i+2;
			p2= (p2>=16) ? p2-16 : p2 ;
			p3= (p3>=16) ? p3-16 : p3 ;			
			int[] currentTris=new int[]{p1,p2,p3,p3,p4,p1};
			currentTris.CopyTo(result,iterator*6);
			iterator++;
		}
		return result;
		
	}
	
	private static int[] createCenterTriangles(){		
		int p1 = 2  ;
		int p2 = 6  ;
		int p3 = 10 ;
		int p4 = 14 ;
		return new int[6]{p1,p2,p3,p3,p4,p1};
		
	}
	
	private static int[] createCornerTriangles ()
	{
		int[] result=new int[24];
		int iterator=0;
		for (int i = 0; i < 16; i+=4) {
			
			int[] currentTris=new int[]{i,i+1,i+2,i+2,i+3,i};
			currentTris.CopyTo(result, iterator*6);
			iterator++;
		}
		return result;
	}
	
	
	
	private static Vector3[] reflectPointsHorizontaly(Vector3[] points){
		Vector3[] result=new Vector3[points.Length];
		for (int i = 0; i < points.Length; i++) {
			result[i]=new Vector3(points[i].x * (-1), points[i].y, points[i].z);
		}
		Vector3 tmp= result[3];
		result[3]=result[1];
		result[1]=tmp;
		
		return result;
	}
	
	private static Vector3[] reflectPointsVerticaly(Vector3[] points){
		Vector3[] result=new Vector3[points.Length];
		for (int i = 0; i < points.Length; i++) {
			result[i]=new Vector3(points[i].x, points[i].y * (-1), points[i].z);
		}
		Vector3 tmp= result[3];
		result[3]=result[1];
		result[1]=tmp;
		
		return result;
	}
	
	
	
	// return array in following order
	// [-1,1][1,1][1,-1][-1,-1]
	private static Vector3[] rectToVectorArray(Rect rect){
		Vector3 tL=new Vector3(rect.x            , rect.y               , 0 );
		Vector3 tR=new Vector3(rect.x+rect.width , rect.y               , 0 );
		Vector3 bR=new Vector3(rect.x+rect.width , rect.y - rect.height , 0 );
		Vector3 bL=new Vector3(rect.x            , rect.y - rect.height , 0 );
		return new Vector3[]{tL,tR,bR,bL};
	}
	
}
