using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

public class UFTMetadataDataCollectionCreator : MonoBehaviour {
	
	
	[MenuItem("Window/UFT Demo/Create GraffitiMetadata array asset")]
	public static void CreateAsset ()
	{
		UFTAtlasMetadata atlasMeta=(UFTAtlasMetadata) AssetDatabase.LoadAssetAtPath("Assets/UFTAtlasEditor/Demo/Atlas/graffiti.asset",typeof(UFTAtlasMetadata));
		if (atlasMeta==null){
			Debug.LogError("cant find atlas metadata");
			return;
		}
			
		List<UFTGraffitiMetadata> metadataArray=new List<UFTGraffitiMetadata>();
		for (int i = 0; i < atlasMeta.entries.Length; i++) {
			metadataArray.Add( new UFTGraffitiMetadata(atlasMeta, atlasMeta.entries[i]));
		}
		
		UFTGrafitiDataCollection grafitiCollection = new UFTGrafitiDataCollection(metadataArray);
		
		AssetDatabase.CreateAsset(grafitiCollection,"Assets/GraffitiMetadataArray.asset");
		AssetDatabase.SaveAssets();
		
	}
}
