using UnityEngine;
using System;

[Serializable]
public class UFTGraffitiMetadata {
	
	[SerializeField]
	public UFTAtlasMetadata atlasMetadata;
	[SerializeField]
	public UFTAtlasEntryMetadata atlasEntryMetadata;	
	
	public UFTGraffitiMetadata (UFTAtlasMetadata atlasMetadata, UFTAtlasEntryMetadata atlasEntryMetadata)
	{
		this.atlasMetadata = atlasMetadata;
		this.atlasEntryMetadata = atlasEntryMetadata;
	}

}
