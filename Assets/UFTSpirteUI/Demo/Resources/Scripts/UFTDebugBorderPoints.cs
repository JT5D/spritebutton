using UnityEngine;
using System.Collections;
using UnityEditor;

[ExecuteInEditMode]
public class UFTDebugBorderPoints : MonoBehaviour {
	public float width       = 200   ;
	public float height      = 100   ;
	public float gizmoRadius = 5     ;
	public bool  showNumbers = false ;
	
	public UFTBorderWidth vertexWidth;
	public UFTBorderWidth uvWidthInPixel;
	
	Mesh tempMesh;
	public UFTAtlasEntryMetadata spriteMetadata;
	
	void Update(){
		tempMesh=UFTFlexibleBorderMeshUtil.createBorderMesh(width,height,vertexWidth,uvWidthInPixel,spriteMetadata,600,433);
		MeshFilter mf=gameObject.GetComponent<MeshFilter>();
		if (mf==null)
			mf=gameObject.AddComponent<MeshFilter>();
		mf.sharedMesh=tempMesh;
		
	}
	
	
	
	
	
	void OnDrawGizmos(){
		
		
		Gizmos.matrix=transform.localToWorldMatrix;
		Gizmos.color = Color.yellow;
		if (tempMesh==null){
			Handles.Label(transform.position, "!!!mesh is empty!!!!");
			return;
		}
		
		
		Vector3[] vertices=tempMesh.vertices;
		Vector2[] uvs=tempMesh.uv;
		for (int i = 0; i < vertices.Length; i++) {			
			Gizmos.DrawSphere(vertices[i], gizmoRadius);
			if (showNumbers)
				Handles.Label(vertices[i],""+i +"\nUV"+ uvs[i]);			
		}
		
		int[] tris=tempMesh.triangles;
		for (int i = 0; i < tris.Length; i+=3) {
			int a1=tris[i];
			int a2=tris[i+1];
			int a3=tris[i+2];
			Gizmos.DrawLine(vertices[a1],vertices[a2]);			
			Gizmos.DrawLine(vertices[a2],vertices[a3]);
			Gizmos.DrawLine(vertices[a3],vertices[a1]);			
		}
		
	}
}
